package edu.westga.cs1302.autodealer.view.output;

import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.Locale;

import edu.westga.cs1302.autodealer.model.Automobile;
import edu.westga.cs1302.autodealer.model.Inventory;

/**
 * The Class ReportGenerator.
 * 
 * @author CS1302
 */
public class ReportGenerator {

	/**
	 * Builds the full summary report of the specified inventory. If inventory is
	 * null, instead of throwing an exception will return a string saying "No
	 * inventory exists.", otherwise builds a summary report of the inventory.
	 * 
	 * @precondition none
	 * @postcondition none
	 * 
	 * @param inventory the inventory
	 *
	 * @return A formatted summary string of the dealership's automobile inventory.
	 */
	public String buildFullSummaryReport(Inventory inventory) {
		String summary = "";
		DecimalFormat df = new DecimalFormat("#,###.000");

		if (inventory == null) {
			summary = "No inventory exists.";
		} else {
			summary = inventory.getDealershipName() + System.lineSeparator();
			summary += "#Automobiles: " + inventory.size() + System.lineSeparator();
		}

		if (inventory.size() > 0) {
			Automobile mostExpensiveAuto = inventory.findMostExpensiveAuto();
			Automobile leastExpensiveAuto = inventory.findLeastExpensiveAuto();
			double averageMiles = inventory.calculateAverageMiles();
			
			summary += System.lineSeparator();

			summary += "Most expensive auto: ";
			summary += this.buildIndividualAutomobileReport(mostExpensiveAuto) + System.lineSeparator();
			summary += "Least expensive auto: ";
			summary += this.buildIndividualAutomobileReport(leastExpensiveAuto) + System.lineSeparator();
			summary += "Average Miles ";
			summary += df.format(averageMiles) + System.lineSeparator();
		}

		return summary;
	}

	private String buildIndividualAutomobileReport(Automobile auto) {
		NumberFormat currencyFormatter = NumberFormat.getCurrencyInstance(Locale.US);

		String output = currencyFormatter.format(auto.getPrice()) + " " + auto.getYear() + " " + auto.getMake() + " "
				+ auto.getModel();
		return output;
	}

}
