package edu.westga.cs1302.autodealer.test.inventory;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

import edu.westga.cs1302.autodealer.model.Automobile;
import edu.westga.cs1302.autodealer.model.Inventory;

class TestFindYearOfNewestAuto {

	@Test
	void testEmptyInventory() {
		Inventory inventory = new Inventory();
		int year = inventory.findYearOfNewestAuto();
		assertEquals(Integer.MIN_VALUE, year);
	}
	
	@Test
	void testOneAutoInventory() {
		Inventory inventory = new Inventory();
		inventory.add(new Automobile("Ford", "Focus", 2008, 108132.2, 6900));
		int year = inventory.findYearOfNewestAuto();
		assertEquals(2008, year);
	}
	
	@Test
	void testMultiAutosNewestFirst() {
		Inventory inventory = new Inventory();
		inventory.add(new Automobile("Ford", "Focus", 2010, 108132.2, 6900));
		inventory.add(new Automobile("Ford", "Focus", 2008, 108132.2, 6900));
		inventory.add(new Automobile("Ford", "Focus", 2009, 108132.2, 6900));
		int year = inventory.findYearOfNewestAuto();
		assertEquals(2010, year);
	}
	
	@Test
	void testMultiAutosNewestMiddle() {
		Inventory inventory = new Inventory();
		inventory.add(new Automobile("Ford", "Focus", 2009, 108132.2, 6900));
		inventory.add(new Automobile("Ford", "Focus", 2010, 108132.2, 6900));
		inventory.add(new Automobile("Ford", "Focus", 2008, 108132.2, 6900));
		int year = inventory.findYearOfNewestAuto();
		assertEquals(2010, year);
	}
	
	@Test
	void testMultiAutosNewestLast() {
		Inventory inventory = new Inventory();
		inventory.add(new Automobile("Ford", "Focus", 2009, 108132.2, 6900));
		inventory.add(new Automobile("Ford", "Focus", 2008, 108132.2, 6900));
		inventory.add(new Automobile("Ford", "Focus", 2010, 108132.2, 6900));
		int year = inventory.findYearOfNewestAuto();
		assertEquals(2010, year);
	}

}
